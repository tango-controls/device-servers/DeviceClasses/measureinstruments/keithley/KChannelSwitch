// ============================================================================
//
// = CONTEXT
//    TANGO Project - Keithley Channel Switch System Support Library
//
// = FILENAME
//    AbstractKChannelSwitch.hpp
//
// = AUTHOR
//    X. Elattaoui
//
//
// $Author: xavela $
//
// $Revision: 1.2 $
//
// ============================================================================

#ifndef _ABSTRACT_K_SWITCH_H_
#define _ABSTRACT_K_SWITCH_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <string>

/**
 *  \addtogroup Keithley Switch System
 *  @{
 */

/**
 *  \brief Abstract class to manage a keithley channel swicth system
 *
 *  \author Xavier Elattaoui
 *  \date 02-2012
 */

class AbstractKChannelSwitch
{
public :
	
	/**
	* Initialization. 
	*/
	//AbstractKChannelSwitch ();
	
	/**
	* Release resources.
	*/
	//virtual ~AbstractKChannelSwitch (void);

	/**
	*	\brief Open a specified channel :
	*/
	virtual void OpenChannel (unsigned short) = 0;

	/**
	*	\brief Open all channels :
	*/
	virtual void OpenAllChannels (void) = 0;

	/**
	*	\brief Close a specified channel :
	*/
	virtual void CloseChannel (unsigned short) = 0;

	/**
	*	\brief Close all channels :
	*/
	virtual void CloseAllChannels (void) = 0;

	/**
	*	\brief Check if the specified channel is opened :
	*/
	virtual bool isChannelOpened (unsigned short) = 0;

	/**
	*	\brief Return the opened input channel :
	*/
  virtual unsigned short GetActiveChannel (void) = 0;

	/**
	*	\brief Store congiguration in a internal memory slot :
	*/
	virtual void SaveConfig (unsigned short) = 0;

	/**
	*	\brief Recall congiguration from a internal memory slot :
	*/
	virtual void RestoreConfig (unsigned short) = 0;

	/**
	*	\brief Number of connected slots :
	*/
  virtual void SetNbConnectedSlots (unsigned short ) = 0;

	/**
	*	\brief Set the active slot :
	*/
  virtual void SetActiveSlotNumber (unsigned short ) = 0;

	/**
	*	\brief Return the active slot :
	*/
  virtual unsigned short GetActiveSlotNumber (void) const = 0;

	/**
	*	\brief Number of connected slot :
	*/
  virtual std::string GetStatus (void) = 0;

protected :
  
private :

};

/** @} */	//- end addtogroup

#endif // _ABSTRACT_K_SWITCH_H_
