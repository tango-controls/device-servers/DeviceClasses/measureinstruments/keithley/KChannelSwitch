/*!
 * \file     K7001.cpp
 * \brief    Class to manage K7001 switch channel system.
 * \author   X. Elattaoui - Synchrotron SOLEIL
 *
 *  NOTE : WARNING !!!
 *  Open means OPEN an internal circuit and CLOSE an input channel 
 */

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "K7001.hpp"
#include <yat/utils/XString.h>

//static const size_t kLO_WATER_MARK = 128;
//static const size_t kHI_WATER_MARK = 512;
//static const size_t kPMSG_PERIOD   = 10000;	//- in ms
//---------------------------------------------------------  
//- the YAT user messages 
//static const size_t kOPEN_CHANNEL       = yat::FIRST_USER_MSG + 10;
//static const size_t kOPEN_ALL_CHANNELS  = yat::FIRST_USER_MSG + 11;
//static const size_t kCLOSE_CHANNEL      = yat::FIRST_USER_MSG + 20;
//static const size_t kCLOSE_ALL_CHANNELS = yat::FIRST_USER_MSG + 21;
//static const size_t kSAVE_CONFIG        = yat::FIRST_USER_MSG + 30;
//static const size_t kRESTORE_CONFIG     = yat::FIRST_USER_MSG + 31;
//static const size_t kIS_CHANNEL_OPENED  = yat::FIRST_USER_MSG + 40;

static const unsigned short MAX_SUPPORTED_SLOTS   = 2;
static const unsigned short MIN_CHANNEL_NUMBER    = 1;
static const unsigned short MAX_CHANNELS_PER_SLOT = 10;
static const unsigned short MAX_MEMORY_SLOTS_IDX  = 9;


// ======================================================================
// K7001::K7001
// ======================================================================
K7001::K7001 (Tango::DeviceImpl* dev, const std::string& devName)
//:   yat::Task(Config(false,		//- enable timeout msg
//							2000,		//- every 2 seconds
//							true,		//- enable periodic msg
//							1000,		//- every second
//							false,		//- lock the internal mutex during msg handling procedure
//							kLO_WATER_MARK,	//- low watermark value
//							kHI_WATER_MARK,	//- high watermark value
//							false,
//							0)),
:		Tango::LogAdapter(dev),
		_gpibDevName(devName),
		_gpibComLink(0),
		_error(""),
		_nb_slots(MAX_SUPPORTED_SLOTS),
		_slot_number(0),
		_init_succeed(false)
{
  //- 
  this->create_proxy();

  //- Close all channels !!!
  this->CloseAllChannels();
}

// ======================================================================
// K7001::~K7001
// ======================================================================
K7001::~K7001 ()
{
  INFO_STREAM << "\n\t K7001::~K7001 -> entering ..." << std::endl;

  this->delete_proxies ();

  INFO_STREAM << "\t K7001::~K7001 -> DONE.\n" << std::endl;
}

// ============================================================================
// K7001::handle_message
// ============================================================================
//void K7001::handle_message (yat::Message& _msg)
//	throw (yat::Exception)
//{
//	//- The Task's lock_ is locked (cf. construction parameter)
//	//-------------------------------------------------------------------
//
//	YAT_TRACE("K7001::handle_message");
//
//	//- handle msg
//	switch (_msg.type())
//	{
//		//- TASK_INIT ----------------------
//	case yat::TASK_INIT:
//		{
//			//- "initialization" code goes here
//			YAT_LOG("K7001::handle_message::TASK_INIT::task is starting up");
//			//- initialise proxy
//			this->_init_succeed = this->create_proxy();
//
//			if ( this->_init_succeed )
//			{
//				this->enable_periodic_msg(true);
//				this->set_periodic_msg_period(kPMSG_PERIOD);
//			}
//			else
//			{
//				//- disable periodic msg
//				this->enable_periodic_msg(false);
//			}
//
//      //- open(or close ?) all channels (per slot)
// 		} 
//		break;
//	//- TASK_EXIT ----------------------
//	case yat::TASK_EXIT:
//    {
//      //- "release" code goes here
//      YAT_LOG("K7001::handle_message::TASK_EXIT::task is quitting");
//
//      //- TODO : close all channels
//
// 			//- "release" code goes here
//			this->delete_proxies();
//
//			INFO_STREAM << "K7001::handle_message handling TASK_EXIT thread is quitting ..." << std::endl;
//   }
//		break;
//	//- TASK_PERIODIC ------------------
//	case yat::TASK_PERIODIC:
//		{
//			//- code relative to the task's periodic job goes here
//			YAT_LOG("K7001::handle_message::handling TASK_PERIODIC msg");
//  
//      //this->_timer.restart();
//
//			//- TODO : update state
//			this->do_periodic_job();
//
//      //- std::cout << "K7001::handle_message::periodic job took " << _timer.elapsed_msec() << " ms" << std::endl;
//		}
//		break;
//	//- TASK_TIMEOUT -------------------
//	case yat::TASK_TIMEOUT:
//		{
//			//- code relative to the task's tmo handling goes here
//			YAT_LOG("K7001::handle_message::handling TASK_TIMEOUT msg");
//		}
//		break;
//  case kOPEN_CHANNEL:
//    {
//			this->open_channel();
//    }
//    break;
//  case kOPEN_ALL_CHANNELS:
//    {
//			this->open_all_channels();
//    }
//    break;
//  case kCLOSE_CHANNEL:
//    {
//			this->close_channel();
//    }
//    break;
//  case kCLOSE_ALL_CHANNELS:
//    {
//			this->close_all_channels();
//    }
//    break;
//  case kSAVE_CONFIG:
//    {
//			this->save_configuration();
//    }
//    break;
//  case kRESTORE_CONFIG:
//    {
//			this->restore_configuration();
//    }
//    break;
//	default:
//		YAT_LOG("K7001::handle_message::unhandled msg type received");
//		break;
//	}
//}

// ======================================================================
// K7001::SetNbConnectedSlots : 
// ======================================================================
void K7001::SetNbConnectedSlots (unsigned short maxSlotsNum)
{
  //- method called only at INIT DEVICE !!!
  if ( !maxSlotsNum || maxSlotsNum > MAX_CHANNELS_PER_SLOT )
  {
    Tango::Except::throw_exception ( ("OUT_OF_RANGE"),
      ("The max supported slot number is 2 !"),
      ("K7001::SetNbConnectedSlots"));
  }
  this->_nb_slots = maxSlotsNum;

  if ( maxSlotsNum < MAX_CHANNELS_PER_SLOT )
    this->_slot_number = 1;
}

// ======================================================================
// K7001::SetActiveSlotNumber
// ======================================================================
void K7001::SetActiveSlotNumber (unsigned short activeSlotNum)
{
  if ( !activeSlotNum || activeSlotNum > _nb_slots || activeSlotNum > MAX_SUPPORTED_SLOTS )
  {
    Tango::Except::throw_exception (("OUT_OF_RANGE"),
      ("The active slot number is 1 or 2 !"),
      ("K7001::SetActiveSlotNumber"));
  }

  this->_slot_number = activeSlotNum;

  //- Before opening a channel, Close all slot's channels
  //std::stringstream cmd;
  //
  //cmd << "ROUT:OPEN (@ " 
  //  << this->_slot_number << "!" << MIN_CHANNEL_NUMBER 
  //  << ":"
  //  << this->_slot_number << "!" << MAX_CHANNELS_PER_SLOT
  //  << ")"
  //  << std::ends;

}

// ======================================================================
// K7001::CloseChannel : opens the internal circuit and so closes the input channel
// ======================================================================
void K7001::CloseChannel (unsigned short chNum)
{
  INFO_STREAM << "K7001::CloseChannel -> this->_slot_number : " << this->_slot_number
    << " & chNum = "
    << chNum
    << std::endl;
  if ( !this->_slot_number )
  {
		Tango::Except::throw_exception (("BAD_PARAMETER"),
			("Please, specify a slot number [1 or 2] !"),
			("K7001::CloseChannel"));
  }

  if ( !chNum || chNum > MAX_CHANNELS_PER_SLOT )
  {
		Tango::Except::throw_exception (("OUT_OF_RANGE"),
			("The channel number must be in the range [1,10] !"),
			("K7001::CloseChannel"));
  }

  std::stringstream cmd;
  INFO_STREAM << "K7001::CloseChannel -> entering : " << std::endl;

  //- Send cmd to OPEN the internal circuit (and so it CLOSEs the channel) !!!
  cmd << "OPEN (@ " << this->_slot_number << "!" << chNum << ")" << std::ends;
  
  //- cmd to send (in the form ":open (@ 1!1, 1!2)" )
  this->write_read(cmd.str());

  INFO_STREAM << "K7001::CloseChannel -> cmd $" << cmd.str() << "$" << std::endl;
	//if ( !this->_init_succeed )
	//{
	//	FATAL_STREAM << "K7001::CloseChannel -> initialisation failed." << std::endl;
	//	Tango::Except::throw_exception (_CPTC ("INIT_ERROR"),
	//		_CPTC ("No gpib proxy created !"),
	//		_CPTC ("K7001::CloseChannel"));
	//}

	//yat::Message * msg = new yat::Message(kCLOSE_CHANNEL, MAX_USER_PRIORITY);
	//if ( !msg )
	//{
	//	ERROR_STREAM << "K7001::CloseChannel -> yat::Message allocation failed." << std::endl;
	//	Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
	//		_CPTC ("yat::Message allocation failed"),
	//		_CPTC ("K7001::CloseChannel"));
	//}

	////- don't wait till the message is processed !!
	//this->post(msg);
}

// ======================================================================
// K7001::CloseAllChannels : 
//    open all internal circuits and so close all input channels
//    for a specific slot
// ======================================================================
void K7001::CloseAllChannels ()
{
  INFO_STREAM << "K7001::CloseAllChannels -> entering : " << std::endl;
  
  //- OPEN ALL internal circuits to CLOSE all input channels
  this->write_read("OPEN ALL");
	//if ( !this->_init_succeed )
	//{
	//	FATAL_STREAM << "K7001::CloseAllChannels -> initialisation failed." << std::endl;
	//	Tango::Except::throw_exception (_CPTC ("INIT_ERROR"),
	//		_CPTC ("No gpib proxy created !"),
	//		_CPTC ("K7001::CloseAllChannels"));
	//}

	//yat::Message * msg = new yat::Message(kCLOSE_ALL_CHANNELS, MAX_USER_PRIORITY);
	//if ( !msg )
	//{
	//	ERROR_STREAM << "K7001::CloseAllChannels -> yat::Message allocation failed." << std::endl;
	//	Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
	//		_CPTC ("yat::Message allocation failed"),
	//		_CPTC ("K7001::CloseAllChannels"));
	//}

	////- don't wait till the message is processed !!
	//this->post(msg);
}

// ======================================================================
// K7001::OpenChannel : closes the internal circuit and so opens the input channel
// ======================================================================
void K7001::OpenChannel (unsigned short chNum)
{
  if ( !chNum || chNum > MAX_CHANNELS_PER_SLOT )
  {
		Tango::Except::throw_exception (("OUT_OF_RANGE"),
			("The channel number must be in the range [1,10] !"),
			("K7001::OpenChannel"));
  }

  //- HERMES specification : open one channel at a time 
  std::stringstream cmd;

  //- CLOSE internal circuit to OPEN the input channel
  cmd << "ROUT:CLOS (@ " << this->_slot_number << "!" << chNum << ")" << std::ends;
  
  INFO_STREAM << "K7001::OpenChannel -> cmd : " << cmd.str() << std::endl;
  
  this->write_read( cmd.str() );
	//if ( !this->_init_succeed )
	//{
	//	FATAL_STREAM << "K7001::OpenChannel -> initialisation failed." << std::endl;
	//	Tango::Except::throw_exception (_CPTC ("INIT_ERROR"),
	//		_CPTC ("No gpib proxy created !"),
	//		_CPTC ("K7001::OpenChannel"));
	//}

	//yat::Message * msg = new yat::Message(kOPEN_CHANNEL, MAX_USER_PRIORITY);
	//if ( !msg )
	//{
	//	ERROR_STREAM << "K7001::OpenChannel -> yat::Message allocation failed." << std::endl;
	//	Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
	//		_CPTC ("yat::Message allocation failed"),
	//		_CPTC ("K7001::OpenChannel"));
	//}

	////- don't wait till the message is processed !!
	//this->post(msg);
}

// ======================================================================
// K7001::OpenAllChannels : closes all internal circuits and so opens all input channels
// ======================================================================
void K7001::OpenAllChannels ()
{
  std::string cmd("ROUT:CLOS (@ ");
  DEBUG_STREAM << "K7001::OpenAllChannels -> entering : SLOT(S) " << this->_nb_slots << std::endl;

  //- open all slot1 channels
  if ( this->_slot_number < MAX_SUPPORTED_SLOTS )
    cmd += "1!1:1!10";
  //- or open slot2 channels (?)
  else
    cmd += "2!1:2!10";

  //- add command terminator
  cmd += ")";

  this->write_read( cmd );
  INFO_STREAM << "K7001::OpenAllChannels -> Closed : " << cmd << std::endl;
	//if ( !this->_init_succeed )
	//{
	//	FATAL_STREAM << "K7001::OpenAllChannels -> initialisation failed." << std::endl;
	//	Tango::Except::throw_exception (_CPTC ("INIT_ERROR"),
	//		_CPTC ("No gpib proxy created !"),
	//		_CPTC ("K7001::OpenAllChannels"));
	//}

	//yat::Message * msg = new yat::Message(kOPEN_ALL_CHANNELS, MAX_USER_PRIORITY);
	//if ( !msg )
	//{
	//	ERROR_STREAM << "K7001::OpenAllChannels -> yat::Message allocation failed." << std::endl;
	//	Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
	//		_CPTC ("yat::Message allocation failed"),
	//		_CPTC ("K7001::OpenAllChannels"));
	//}

	////- don't wait till the message is processed !!
	//this->post(msg);
}

// ======================================================================
// K7001::isChannelOpened
// ======================================================================
bool K7001::isChannelOpened (unsigned short chNum)
{
  bool chOpened = false;
  std::string response("");
  std::stringstream cmd; //- format : "ROUT:CLOS? (@slotNum!chNum)" -> returns 1 : closed and 0 : opened

  if ( !chNum || chNum > MAX_CHANNELS_PER_SLOT )
  {
		Tango::Except::throw_exception (("OUT_OF_RANGE"),
			("The channel number must be in the range [1,10] !"),
			("K7001::isChannelOpened"));
  }

  cmd << "ROUT:CLOS? (@" << this->_slot_number << "!" << chNum << ")" << std::ends;
  
  response = this->write_read( cmd.str() );

  chOpened = yat::XString<bool>::to_num (response);

	//if ( !this->_init_succeed )
	//{
	//	FATAL_STREAM << "K7001::isChannelOpened -> initialisation failed." << std::endl;
	//	Tango::Except::throw_exception (_CPTC ("INIT_ERROR"),
	//		_CPTC ("No gpib proxy created !"),
	//		_CPTC ("K7001::isChannelOpened"));
	//}

	//yat::Message * msg = new yat::Message(kIS_CHANNEL_OPENED, MAX_USER_PRIORITY);
	//if ( !msg )
	//{
	//	ERROR_STREAM << "K7001::isChannelOpened -> yat::Message allocation failed." << std::endl;
	//	Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
	//		_CPTC ("yat::Message allocation failed"),
	//		_CPTC ("K7001::isChannelOpened"));
	//}

	////- don't wait till the message is processed !!
	//this->post(msg);

  return (chOpened);
}

// ======================================================================
// K7001::GetActiveChannel
// ======================================================================
unsigned short K7001::GetActiveChannel()
{
  std::stringstream patternTofind;      //- to extract the chNum(xxx), find (@SLOT_NUM!xxx)
  std::string noOpenedChannel("(@)");   //- returns this if no input channel is opened
  std::string cmd ("ROUT:CLOS:STAT?");  //- cmd to get opened channel(s). The response is in the form "(@SLOT_NUM!CH_NUM).
  std::string resp = this->write_read( cmd );
  unsigned short inputChannelNum = 0;
  
  if ( resp.find(noOpenedChannel) != std::string::npos )
    inputChannelNum = 0;
  else
  {
    patternTofind << this->_slot_number << "!" << std::ends;
    size_t chNumPosBeg = resp.find( patternTofind.str().c_str() );

    if ( chNumPosBeg != std::string::npos )
    {
      size_t chNumPosEnd = resp.string::find_first_of(",)", chNumPosBeg+1);
      std::string subStr = resp.substr(chNumPosBeg + patternTofind.str().size()-1, ( chNumPosEnd - ( chNumPosBeg+patternTofind.str().size()-1 )) );
//std::cout << "\n\tK7001::GetActiveChannel -> resp =$" << resp << "$"
//          << "\n\tsubStr = $" << subStr << "$"
//          << " \n\tchNumPosBeg = " << chNumPosBeg
//          << " \n\tchNumPosEnd = " << chNumPosEnd
//          << std::endl;
      inputChannelNum = yat::XString<unsigned short>::to_num (subStr);
    }
  }
 
  return inputChannelNum;
}

// ======================================================================
// K7001::SaveConfig
// ======================================================================
void K7001::SaveConfig (unsigned short slotNum)
{
  if ( slotNum > MAX_MEMORY_SLOTS_IDX )
  {
		Tango::Except::throw_exception (("OUT_OF_RANGE"),
			("The slot number is above the maximum supported !"),
			("K7001::SaveConfig"));
  }

  std::stringstream cmd;
  INFO_STREAM << "K7001::save_configuration -> entering : " << std::endl;

  cmd << "*SAV " << slotNum << std::ends;

  this->write_read( cmd.str() );
	//if ( !this->_init_succeed )
	//{
	//	FATAL_STREAM << "K7001::SaveConfig -> initialisation failed." << std::endl;
	//	Tango::Except::throw_exception (_CPTC ("INIT_ERROR"),
	//		_CPTC ("No gpib proxy created !"),
	//		_CPTC ("K7001::SaveConfig"));
	//}

	//yat::Message * msg = new yat::Message(kSAVE_CONFIG, MAX_USER_PRIORITY);
	//if ( !msg )
	//{
	//	ERROR_STREAM << "K7001::SaveConfig -> yat::Message allocation failed." << std::endl;
	//	Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
	//		_CPTC ("yat::Message allocation failed"),
	//		_CPTC ("K7001::SaveConfig"));
	//}

	////- don't wait till the message is processed !!
	//this->post(msg);
}

// ======================================================================
// K7001::RestoreConfig
// ======================================================================
void K7001::RestoreConfig (unsigned short slotNum)
{
  if ( slotNum > MAX_MEMORY_SLOTS_IDX )
  {
		Tango::Except::throw_exception (("OUT_OF_RANGE"),
			("The slot number is above the maximum supported !"),
			("K7001::RestoreConfig"));
  }

  std::stringstream cmd;
  INFO_STREAM << "K7001::restore_configuration -> entering : " << std::endl;

  cmd << "*RCL " << slotNum << std::ends;
  
  this->write_read( cmd.str() );
	//if ( !this->_init_succeed )
	//{
	//	FATAL_STREAM << "K7001::RestoreConfig -> initialisation failed." << std::endl;
	//	Tango::Except::throw_exception (_CPTC ("INIT_ERROR"),
	//		_CPTC ("No gpib proxy created !"),
	//		_CPTC ("K7001::RestoreConfig"));
	//}

	//yat::Message * msg = new yat::Message(kRESTORE_CONFIG, MAX_USER_PRIORITY);
	//if ( !msg )
	//{
	//	ERROR_STREAM << "K7001::RestoreConfig -> yat::Message allocation failed." << std::endl;
	//	Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
	//		_CPTC ("yat::Message allocation failed"),
	//		_CPTC ("K7001::RestoreConfig"));
	//}

	////- don't wait till the message is processed !!
	//this->post(msg);
}

// ============================================================================
// K7001::GetStatus :
// ============================================================================
std::string K7001::GetStatus()
{
  std::string status("");

  if ( !_gpibComLink )
    status += "No gpib communication found !";

  //- get error
  std::string cmd("SYST:ERR?");
  status += this->write_read( cmd );

  return status;
}

// ============================================================================
//                            - INTERNAL METHODS -
// ============================================================================

// ============================================================================
// K7001::create_proxy :
// ============================================================================
bool K7001::create_proxy ()
{
	bool success = false;
  try
  {
    _gpibComLink = new Tango::DeviceProxyHelper(this->_gpibDevName.c_str());
		success = true;
		this->_error = "Init done.";
    DEBUG_STREAM << "K7001::create_proxy : proxy on \"" << this->_gpibDevName << "\" created." << std::endl;
  }
  catch(...)
  {
    if (_gpibComLink)
    {
      delete _gpibComLink;
      _gpibComLink = 0;
    }
		this->_error = "K7001::create_proxy : Failed to create proxy on \"" + this->_gpibDevName + "\"";
    FATAL_STREAM << this->_error << std::endl;
  }
	return success;
}

// ============================================================================
// K7001::delete_proxies :
// ============================================================================
void K7001::delete_proxies ()
{
  if (this->_gpibComLink)
  {
    delete _gpibComLink;
    _gpibComLink = 0;
  }
}

//// ============================================================================
//// K7001::do_periodic_job :
//// ============================================================================
//void K7001::do_periodic_job ()
//{
//  INFO_STREAM << "K7001::do_periodic_job -> entering : " << std::endl;
//  //- TODO : check errors and update state/status
//}
//
//// ============================================================================
//// K7001::open_channel :
//// ============================================================================
//void K7001::open_channel ()
//{
//  std::stringstream cmd;
//  INFO_STREAM << "K7001::open_channel -> entering : " << std::endl;
//
//  cmd << "OPEN (@ " << this->_slot_number << "!" << this->_channel_number << ")" << std::ends;
//  
//  //- cmd to send (in the form ":open (@ 1!1, 1!2)" )
//  this->write_read(cmd.str());
//}
//
//// ============================================================================
//// K7001::open_all_channels :
//// ============================================================================
//void K7001::open_all_channels ()
//{
//  INFO_STREAM << "K7001::open_all_channels -> entering : " << std::endl;
//  
//  this->write_read("OPEN ALL");
//}
//
//// ============================================================================
//// K7001::close_channel :
//// ============================================================================
//void K7001::close_channel ()
//{
//  std::stringstream cmd;
//  INFO_STREAM << "K7001::close_channel -> entering : " << std::endl;
//
//  cmd << "ROUT:CLOS (@ " << this->_slot_number << "!" << this->_channel_number << ")" << std::ends;
//  
//  this->write_read( cmd.str() );
//}
//
//// ============================================================================
//// K7001::close_all_channels :
//// ============================================================================
//void K7001::close_all_channels ()
//{
//  std::string cmd("ROUT:CLOS (@ ");
//  INFO_STREAM << "K7001::close_all_channels -> entering : " << std::endl;
//
//  //- close all slot1 channels
//  cmd += "1!1:1!10";
//  //- have to close slot2 channels (?)
//  if ( this->_slot_number == MAX_SUPPORTED_SLOTS )
//    cmd += ", 2!1:2!10";
//
//  //- add command terminator
//  cmd += ")";
//
//  this->write_read( cmd );
//}
//
//// ============================================================================
//// K7001::save_configuration :
//// ============================================================================
//void K7001::save_configuration ()
//{
//  std::stringstream cmd;
//  INFO_STREAM << "K7001::save_configuration -> entering : " << std::endl;
//
//  cmd << "*SAV " << slotNum << std::ends;
//
//  this->write_read( cmd.str() );
//}
//
//// ============================================================================
//// K7001::restore_configuration :
//// ============================================================================
//void K7001::restore_configuration ()
//{
//  std::stringstream cmd;
//  INFO_STREAM << "K7001::restore_configuration -> entering : " << std::endl;
//
//  cmd << "*RCL " << slotNum << std::ends;
//  
//  this->write_read( cmd.str() );
//}

// ============================================================================
// K7001::write_read :
// ============================================================================
std::string K7001::write_read (const std::string& cmd)
{
	std::string response("");
  //INFO_STREAM << "K7001::write_read -> writing cmd : *" << cmd << "*" << std::endl;

	//- this case cannot be reached !!
	if ( !_gpibComLink )
	{
    this->_error = "K7001::write_read : Device initialisation failed -> no proxy created to write/read commands !";
    FATAL_STREAM << this->_error << std::endl;
		return response;
	}

  try
  {
	bool readResponse = false;
	if ( cmd.rfind("?") != std::string::npos )
		readResponse = true;

	if ( readResponse )
		this->_gpibComLink->command_inout("WriteRead", cmd, response);
	else
		this->_gpibComLink->command_in("Write", cmd);
  }
  catch(Tango::DevFailed & df)
  {
    ERROR_STREAM << "K7001::write_read -> cannot send command or read response for : \"" << cmd << "\"" << std::endl;
		
    Tango::Except::re_throw_exception (df,
      ("INTERNAL_ERROR"),
			("Cannot write or read from Gpib !"),
			("K7001::write_read"));
  }
  catch(...)
  {
    ERROR_STREAM << "K7001::write_read -> cannot send command or read response for : \"" << cmd << "\"" << std::endl;
    
    Tango::Except::throw_exception (
      ("INTERNAL_ERROR"),
			("Cannot write or read from Gpib !"),
			("K7001::write_read"));
  }
  //INFO_STREAM << "K7001::write_read -> DONE." << std::endl;

	return response;
}

