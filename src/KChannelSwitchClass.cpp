static const char *ClassId    = "$Id: KChannelSwitchClass.cpp,v 1.2 2012-05-15 10:06:44 xavela Exp $";
static const char *TagName    = "$Name: not supported by cvs2svn $";
static const char *CvsPath    = "$Source: /users/chaize/newsvn/cvsroot/Instrumentation/KChannelSwitch/src/KChannelSwitchClass.cpp,v $";
static const char *SvnPath    = "$HeadURL: $";
static const char *HttpServer = "http://www.esrf.fr/computing/cs/tango/tango_doc/ds_doc/";
//+=============================================================================
//
// file :        KChannelSwitchClass.cpp
//
// description : C++ source for the KChannelSwitchClass. A singleton
//               class derived from DeviceClass. It implements the
//               command list and all properties and methods required
//               by the KChannelSwitch once per process.
//
// project :     TANGO Device Server
//
// $Author: xavela $
//
// $Revision: 1.2 $
//
// $Log: not supported by cvs2svn $
//
// copyleft :   European Synchrotron Radiation Facility
//              BP 220, Grenoble 38043
//              FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================


#include <tango.h>

#include <KChannelSwitch.h>
#include <KChannelSwitchClass.h>


//+----------------------------------------------------------------------------
/**
 *	Create KChannelSwitchClass singleton and return it in a C function for Python usage
 */
//+----------------------------------------------------------------------------
extern "C" {
#ifdef WIN32

__declspec(dllexport)

#endif

	Tango::DeviceClass *_create_KChannelSwitch_class(const char *name) {
		return KChannelSwitch_ns::KChannelSwitchClass::init(name);
	}
}


namespace KChannelSwitch_ns
{
//+----------------------------------------------------------------------------
//
// method : 		OpenAllChannelsCmd::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be executed
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *OpenAllChannelsCmd::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "OpenAllChannelsCmd::execute(): arrived" << endl;

	((static_cast<KChannelSwitch *>(device))->open_all_channels());
	return new CORBA::Any();
}

//+----------------------------------------------------------------------------
//
// method : 		CloseAllChannelsClass::execute()
// 
// description : 	method to trigger the execution of the command.
//                PLEASE DO NOT MODIFY this method core without pogo   
//
// in : - device : The device on which the command must be executed
//		- in_any : The command input data
//
// returns : The command output data (packed in the Any object)
//
//-----------------------------------------------------------------------------
CORBA::Any *CloseAllChannelsClass::execute(Tango::DeviceImpl *device,const CORBA::Any &in_any)
{

	cout2 << "CloseAllChannelsClass::execute(): arrived" << endl;

	((static_cast<KChannelSwitch *>(device))->close_all_channels());
	return new CORBA::Any();
}



//
//----------------------------------------------------------------
//	Initialize pointer for singleton pattern
//----------------------------------------------------------------
//
KChannelSwitchClass *KChannelSwitchClass::_instance = NULL;

//+----------------------------------------------------------------------------
//
// method : 		KChannelSwitchClass::KChannelSwitchClass(string &s)
// 
// description : 	constructor for the KChannelSwitchClass
//
// in : - s : The class name
//
//-----------------------------------------------------------------------------
KChannelSwitchClass::KChannelSwitchClass(string &s):DeviceClass(s)
{

	cout2 << "Entering KChannelSwitchClass constructor" << endl;
	set_default_property();
	get_class_property();
	write_class_property();
	
	cout2 << "Leaving KChannelSwitchClass constructor" << endl;

}
//+----------------------------------------------------------------------------
//
// method : 		KChannelSwitchClass::~KChannelSwitchClass()
// 
// description : 	destructor for the KChannelSwitchClass
//
//-----------------------------------------------------------------------------
KChannelSwitchClass::~KChannelSwitchClass()
{
	_instance = NULL;
}

//+----------------------------------------------------------------------------
//
// method : 		KChannelSwitchClass::instance
// 
// description : 	Create the object if not already done. Otherwise, just
//			return a pointer to the object
//
// in : - name : The class name
//
//-----------------------------------------------------------------------------
KChannelSwitchClass *KChannelSwitchClass::init(const char *name)
{
	if (_instance == NULL)
	{
		try
		{
			string s(name);
			_instance = new KChannelSwitchClass(s);
		}
		catch (bad_alloc)
		{
			throw;
		}		
	}		
	return _instance;
}

KChannelSwitchClass *KChannelSwitchClass::instance()
{
	if (_instance == NULL)
	{
		cerr << "Class is not initialised !!" << endl;
		exit(-1);
	}
	return _instance;
}

//+----------------------------------------------------------------------------
//
// method : 		KChannelSwitchClass::command_factory
// 
// description : 	Create the command object(s) and store them in the 
//			command list
//
//-----------------------------------------------------------------------------
void KChannelSwitchClass::command_factory()
{
	command_list.push_back(new CloseAllChannelsClass("CloseAllChannels",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"",
		"",
		Tango::EXPERT));
	command_list.push_back(new OpenAllChannelsCmd("OpenAllChannels",
		Tango::DEV_VOID, Tango::DEV_VOID,
		"",
		"",
		Tango::EXPERT));

	//	add polling if any
	for (unsigned int i=0 ; i<command_list.size(); i++)
	{
	}
}

//+----------------------------------------------------------------------------
//
// method : 		KChannelSwitchClass::get_class_property
// 
// description : 	Get the class property for specified name.
//
// in :		string	name : The property name
//
//+----------------------------------------------------------------------------
Tango::DbDatum KChannelSwitchClass::get_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_prop.size() ; i++)
		if (cl_prop[i].name == prop_name)
			return cl_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}
//+----------------------------------------------------------------------------
//
// method : 		KChannelSwitchClass::get_default_device_property()
// 
// description : 	Return the default value for device property.
//
//-----------------------------------------------------------------------------
Tango::DbDatum KChannelSwitchClass::get_default_device_property(string &prop_name)
{
	for (unsigned int i=0 ; i<dev_def_prop.size() ; i++)
		if (dev_def_prop[i].name == prop_name)
			return dev_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}

//+----------------------------------------------------------------------------
//
// method : 		KChannelSwitchClass::get_default_class_property()
// 
// description : 	Return the default value for class property.
//
//-----------------------------------------------------------------------------
Tango::DbDatum KChannelSwitchClass::get_default_class_property(string &prop_name)
{
	for (unsigned int i=0 ; i<cl_def_prop.size() ; i++)
		if (cl_def_prop[i].name == prop_name)
			return cl_def_prop[i];
	//	if not found, return  an empty DbDatum
	return Tango::DbDatum(prop_name);
}
//+----------------------------------------------------------------------------
//
// method : 		KChannelSwitchClass::device_factory
// 
// description : 	Create the device object(s) and store them in the 
//			device list
//
// in :		Tango::DevVarStringArray *devlist_ptr : The device name list
//
//-----------------------------------------------------------------------------
void KChannelSwitchClass::device_factory(const Tango::DevVarStringArray *devlist_ptr)
{

	//	Create all devices.(Automatic code generation)
	//-------------------------------------------------------------
	for (unsigned long i=0 ; i < devlist_ptr->length() ; i++)
	{
		cout4 << "Device name : " << (*devlist_ptr)[i].in() << endl;
						
		// Create devices and add it into the device list
		//----------------------------------------------------
		device_list.push_back(new KChannelSwitch(this, (*devlist_ptr)[i]));							 

		// Export device to the outside world
		// Check before if database used.
		//---------------------------------------------
		if ((Tango::Util::_UseDb == true) && (Tango::Util::_FileDb == false))
			export_device(device_list.back());
		else
			export_device(device_list.back(), (*devlist_ptr)[i]);
	}
	//	End of Automatic code generation
	//-------------------------------------------------------------

}
//+----------------------------------------------------------------------------
//	Method: KChannelSwitchClass::attribute_factory(vector<Tango::Attr *> &att_list)
//-----------------------------------------------------------------------------
void KChannelSwitchClass::attribute_factory(vector<Tango::Attr *> &att_list)
{
	//	Attribute : inputChannelNumber
	inputChannelNumberAttrib	*input_channel_number = new inputChannelNumberAttrib();
	Tango::UserDefaultAttrProp	input_channel_number_prop;
	input_channel_number_prop.set_label("input channel number");
	input_channel_number_prop.set_unit(" ");
	input_channel_number_prop.set_standard_unit(" ");
	input_channel_number_prop.set_display_unit(" ");
	input_channel_number_prop.set_format("%2d");
	input_channel_number_prop.set_max_value("10");
	input_channel_number_prop.set_min_value("1");
	input_channel_number_prop.set_description("Input channel number to open (from 1 to 10).");
	input_channel_number->set_default_properties(input_channel_number_prop);
	input_channel_number->set_memorized();
	input_channel_number->set_memorized_init(false);
	att_list.push_back(input_channel_number);

	//	End of Automatic code generation
	//-------------------------------------------------------------
}

//+----------------------------------------------------------------------------
//
// method : 		KChannelSwitchClass::get_class_property()
// 
// description : 	Read the class properties from database.
//
//-----------------------------------------------------------------------------
void KChannelSwitchClass::get_class_property()
{
	//	Initialize your default values here (if not done with  POGO).
	//------------------------------------------------------------------

	//	Read class properties from database.(Automatic code generation)
	//------------------------------------------------------------------

	//	Call database and extract values
	//--------------------------------------------
	if (Tango::Util::instance()->_UseDb==true)
		get_db_class()->get_property(cl_prop);
	Tango::DbDatum	def_prop;
	int	i = -1;


	//	End of Automatic code generation
	//------------------------------------------------------------------

}

//+----------------------------------------------------------------------------
//
// method : 	KChannelSwitchClass::set_default_property
// 
// description: Set default property (class and device) for wizard.
//              For each property, add to wizard property name and description
//              If default value has been set, add it to wizard property and
//              store it in a DbDatum.
//
//-----------------------------------------------------------------------------
void KChannelSwitchClass::set_default_property()
{
	string	prop_name;
	string	prop_desc;
	string	prop_def;

	vector<string>	vect_data;
	//	Set Default Class Properties
	//	Set Default Device Properties
	prop_name = "GpibDeviceName";
	prop_desc = "GPIB device proxy name.";
	prop_def  = "";
	vect_data.clear();
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

	prop_name = "NbConnectedSlot";
	prop_desc = "The number of slots connected in the Keithley Channel Switch System.";
	prop_def  = "1";
	vect_data.clear();
	vect_data.push_back("1");
	if (prop_def.length()>0)
	{
		Tango::DbDatum	data(prop_name);
		data << vect_data ;
		dev_def_prop.push_back(data);
		add_wiz_dev_prop(prop_name, prop_desc,  prop_def);
	}
	else
		add_wiz_dev_prop(prop_name, prop_desc);

}
//+----------------------------------------------------------------------------
//
// method : 		KChannelSwitchClass::write_class_property
// 
// description : 	Set class description as property in database
//
//-----------------------------------------------------------------------------
void KChannelSwitchClass::write_class_property()
{
	//	First time, check if database used
	//--------------------------------------------
	if (Tango::Util::_UseDb == false)
		return;

	Tango::DbData	data;
	string	classname = get_name();
	string	header;
	string::size_type	start, end;

	//	Put title
	Tango::DbDatum	title("ProjectTitle");
	string	str_title("Keithley Channel Switch system");
	title << str_title;
	data.push_back(title);

	//	Put Description
	Tango::DbDatum	description("Description");
	vector<string>	str_desc;
	str_desc.push_back("Device which controls a channel switch system <Br>");
	str_desc.push_back("Supported model(s) : 7001 <Br>");
	description << str_desc;
	data.push_back(description);
		
	//	put cvs or svn location
	string	filename(classname);
	filename += "Class.cpp";
	
	// Create a string with the class ID to
	// get the string into the binary
	string	class_id(ClassId);
	
	// check for cvs information
	string	src_path(CvsPath);
	start = src_path.find("/");
	if (start!=string::npos)
	{
		end   = src_path.find(filename);
		if (end>start)
		{
			string	strloc = src_path.substr(start, end-start);
			//	Check if specific repository
			start = strloc.find("/cvsroot/");
			if (start!=string::npos && start>0)
			{
				string	repository = strloc.substr(0, start);
				if (repository.find("/segfs/")!=string::npos)
					strloc = "ESRF:" + strloc.substr(start, strloc.length()-start);
			}
			Tango::DbDatum	cvs_loc("cvs_location");
			cvs_loc << strloc;
			data.push_back(cvs_loc);
		}
	}
	// check for svn information
	else
	{
		string	src_path(SvnPath);
		start = src_path.find("://");
		if (start!=string::npos)
		{
			end = src_path.find(filename);
			if (end>start)
			{
				header = "$HeadURL: ";
				start = header.length();
				string	strloc = src_path.substr(start, (end-start));
				
				Tango::DbDatum	svn_loc("svn_location");
				svn_loc << strloc;
				data.push_back(svn_loc);
			}
		}
	}

	//	Get CVS or SVN revision tag
	
	// CVS tag
	string	tagname(TagName);
	header = "$Name: ";
	start = header.length();
	string	endstr(" $");
	
	end   = tagname.find(endstr);
	if (end!=string::npos && end>start)
	{
		string	strtag = tagname.substr(start, end-start);
		Tango::DbDatum	cvs_tag("cvs_tag");
		cvs_tag << strtag;
		data.push_back(cvs_tag);
	}
	
	// SVN tag
	string	svnpath(SvnPath);
	header = "$HeadURL: ";
	start = header.length();
	
	end   = svnpath.find(endstr);
	if (end!=string::npos && end>start)
	{
		string	strloc = svnpath.substr(start, end-start);
		
		string tagstr ("/tags/");
		start = strloc.find(tagstr);
		if ( start!=string::npos )
		{
			start = start + tagstr.length();
			end   = strloc.find(filename);
			string	strtag = strloc.substr(start, end-start-1);
			
			Tango::DbDatum	svn_tag("svn_tag");
			svn_tag << strtag;
			data.push_back(svn_tag);
		}
	}

	//	Get URL location
	string	httpServ(HttpServer);
	if (httpServ.length()>0)
	{
		Tango::DbDatum	db_doc_url("doc_url");
		db_doc_url << httpServ;
		data.push_back(db_doc_url);
	}

	//  Put inheritance
	Tango::DbDatum	inher_datum("InheritedFrom");
	vector<string> inheritance;
	inheritance.push_back("Device_4Impl");
	inher_datum << inheritance;
	data.push_back(inher_datum);

	//	Call database and and values
	//--------------------------------------------
	get_db_class()->put_property(data);
}

}	// namespace
