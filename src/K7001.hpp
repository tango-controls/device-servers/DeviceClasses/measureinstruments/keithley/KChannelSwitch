// ============================================================================
//
// = CONTEXT
//    TANGO Project - Keithley Channel Switch System Support Library
//
// = FILENAME
//    K7001.hpp
//
// = AUTHOR
//    X. Elattaoui
//
//
// $Author: xavela $
//
// $Revision: 1.2 $
//
// ============================================================================

#ifndef _K_7001_CHANNEL_SWITCH_H_
#define _K_7001_CHANNEL_SWITCH_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include "AbstractKChannelSwitch.hpp"
//#include <yat/threading/Task.h>
#include <tango.h>
#include <helpers/DeviceProxyHelper.h>

/**
 *  \addtogroup Keithley Switch System
 *  @{
 */

/**
 *  \brief Abstract class to manage a keithley 7001 channel swicth system
 *
 *  \author Xavier Elattaoui
 *  \date 02-2012
 */
class K7001 : public AbstractKChannelSwitch, public Tango::LogAdapter//, public yat::Task
{
public :
	
	/**
	* Initialization. 
	*/
	K7001 (Tango::DeviceImpl* dev, const std::string& communicationDevName);
	
	/**
	* Release resources.
	*/
	virtual ~K7001 (void);

	/**
	*	\brief Open a specified channel :
	*/
	void OpenChannel (unsigned short chNum);

	/**
	*	\brief Open all channels :
	*/
	void OpenAllChannels (void);

	/**
	*	\brief Close a specified channel :
	*/
	void CloseChannel (unsigned short chNum);

	/**
	*	\brief Close all channels :
	*/
	void CloseAllChannels (void);

	/**
	*	\brief Check if the specified channel is opened :
	*/
	bool isChannelOpened(unsigned short chNum);

	/**
	*	\brief Return the opened input channel :
	*/
  unsigned short GetActiveChannel (void);

	/**
	*	\brief Store congiguration in a device slot :
	*/
	void SaveConfig(unsigned short slotNum);

	/**
	*	\brief Recall congiguration from a device slot :
	*/
	void RestoreConfig(unsigned short slotNum);

	/**
	*	\brief Set the number of connected slot(s) :
	*/
  void SetNbConnectedSlots (unsigned short );

	/**
	*	\brief Set the active slot(s) :
	*/
  void SetActiveSlotNumber (unsigned short );

	/**
	*	\brief Return the active slot(s) :
	*/
  unsigned short GetActiveSlotNumber () const {
    return this->_slot_number;
  }

	/**
	*	\brief Return error(s) if any :
	*/
  std::string GetStatus ();

protected :
   
	//yat::Mutex	_mutex;	//- mutex

	//- handle_message -----------------------
	//virtual void handle_message (yat::Message& msg)
	//	throw (yat::Exception);
  
private :
	bool create_proxy();
	void delete_proxies();
	//void do_periodic_job();
	//void open_channel();
	//void open_all_channels();
	//void close_channel();
	//void close_all_channels();
	//void save_configuration();
	//void restore_configuration();

  std::string write_read (const std::string& cmd);


	std::string         _gpibDevName;
  Tango::DeviceProxyHelper* _gpibComLink;
	std::string         _error;
	unsigned short      _nb_slots;
	unsigned short      _slot_number;
	bool								_init_succeed;

};

/** @} */	//- end addtogroup

#endif // _K_7001_CHANNEL_SWITCH_H_
